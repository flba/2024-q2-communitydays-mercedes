**SDV WG Q2/Community Days**

When: 5-6 June, 2024

Registration:
Register on [website](https://www.eclipse-foundation.events/event/SDVCommunityDayJune2024/summary)

Venue: 
Mercedes-Benz Customer Center Sindelfingen

**Day 1: June 5th**

Program runs in hybrid form: 9:00-17:00

[on-site only] Evening meet & greet

| Time | Session Name | Presenter | 
| ------ | ------ | ------ | 
| 8:30 - 9:00 |Registration|  
| 9:00-9:30 | [Hybrid] **Welcome and opening remarks** | EF SDV Team and local host |
| 9:00-9:15 | Opening remarks & Eclipse ThreadX SIG update | Dana & Sara (EF) |
| 9:15-9:30 | Welcome from Mercedes-Benz |  Jochen Strenkert (Mercedes-Benz) |
| 9:30-10:30 | [Hybrid] **Tech Talks - Rust Session** | 1 h |
| 9:30 - 10 | Creating a Rust SIG within the SDV WG | Florian Gilcher (Ferrous Systems)
| 10 - 10:30| cancelled ~~Rust - certify the Rust core~~ | ~~Amit Dharmapurikar (Thoughtworks)~~|
| 10 - 10:30| A 2-year journey of implementing a cluster instrument in Rust on a platform not supported by Rust – and open sourcing some parts on the way |  Florian Bartels (Elektrobit)|
| 10:30-11:00 | **Coffee break 30min** | 
| 11:00-12:00 | [Hybrid] **Tech Talks** | 1 h |
| 11:00 - 11:30| Another Message from the other side - Essentials of an Automotive Cloud | Thorben Krieger, Daniel Elhs (Valtech Mobility) |
| 11:30 - 12:00 | Eclipse SDV Hardware Reference Platform – A Proposal | Thilo Schmitt (MBTI) | 
| 12:00 - 13:30 | **Lunch** & visit to Demo car |  1h30 | 
| 13:30 - 15:30 | [Hybrid] **New Projects and Updates** | 2 h |
| 13:30 - 14:00 | Project Update: Eclipse openDuT  | Michael Renz (MBTI) |
| 14:00 - 14:30| Project update: Introducing the Ankaios Dashboard |  Mesut-Ömür Özden, Felix Mölders, and Hendrik Supper (d-fine GmbH) |
| 14:30 - 15:00 | New Project: Eclipse Piccolo | Chulhee (LGE) &  Punithan Xavier (LGE) |
| 15:00 - 15:30 | New Project: Eclipse Zenoh-Flow | Phani Gangula & Julien Loudet (Zettascale)  |
| 15:30 - 16:00|  **Coffee break 30min** | |
| 16:00 - 18:00 | [Hybrid] **Tech Talks** | 2 h |
| 16:00 - 16:30| Bring digital twins into the vehicle, a demo approach with Eclipse Ibeji | Ash Beitz and Hans Sperker (MS)| 
| 17:00 - 17:30|  SDV Toolchains | Daniel Lueddecke (Harman) Filipe Prezado (Microsoft) |
| 17:30 - 18:00 | Eclipse IDE WG actitivities | Thomas Froment (EF) |
| 18:00-19:00 | [on site only] **Meet & Greet Reception** | 
 
**Day 2: June 6th**

Morning workshop (on site only) 9:00-12:00 + Lunch 

| Time | Session Name | Presenter | 
| ------ | ------ | ------ | 
| 8:30-9:00 |Registration & Coffee|  
| 9:00-11:30 | [on-site only] **open collaboration 2.5 Hours** |
| 9:00-10:00| _Room 1_ Software Orchestration Blueprint with Symphony – updates and open discussion | Elektrobit, RedHat, Microsoft (Haishi Bai and Filipe Prezado)
| 10:00-10:30 | [on-site only] **coffee break 0.5 Hours** |
| 10:30-11:30 | _Room 1_ SDV Survey content and report & roundtable |  Polly (EF) |
| 9:00-10:00| _Room 2_ TBD Demonstrator of the Cluster Instruments | Elektrobit   |
| 10:00-10:30 | [on-site only] **coffee break 0.5 Hours** |
| 10:30-11:30 | _Room 2_ TBD Rust roundtable || 
| 11:30-12:00 | [on-site only] **open collaboration wrap up 0.5 hours** | 
| 12:00-13:00 | **Lunch break 1 hour** |

Talk line up:
- [x] 15 mins on ThreadX SIG and Alliance - Dana 
- [x] Possible new project proposal Eclipse Zenoh-Flow - Phani Gangula (Zettascale) 
- [x] Rust - certify the Rust core - Amit Dharmapurikar (Thoughtworks)
- [x] more on Rust SIG Florian (Ferrous Systems)
- [x] MBTI activities - Thilo (MBTI)
- [ ] MBTI activities Demo Car ? -  Thilo (MBTI)
- [x] MBTI - bring digital twins into the vehicle, a demo approach with Eclipse Ibeji - Hans (MS), Christian (MB), Ash (MS) 
- [x] New Project Piccolo - Chulhee (LGE) &  Punithan Xavier (LGE)
- [x] "A 2-year journey of implementing a cluster instrument in Rust on a platform not supported by Rust – and open sourcing some parts on the way"  Florian Bartels (EB)
- [x] Demonstrator of the Cluster Instruments EB 
- [ ] tbd (AWS)
- [x] Toolchains Daniel (Harman) Filipe (MS)
- [x] Another Message from the other side - Essentials of an Automotive Cloud Valtech Mobility 
- [x] Ankaios Dashboard Mesut-Ömür Özden (d-fine GmbH)
- [ ] Panel on community collaboration 
- [ ] talk from FOSS evangelist (tbd) 
- [x] Capella/Theia workshop 
- [x] SDV Survey content and report - Polly (EF) 




Open points:

- [ ] Frederic Desbiens (Eclipse Foundation) - Eclipse ThreadX - POSTPONE
- [ ] Eclipse Theia - practical usage example / demo for SDV community - POSTPONE
- [ ] Eclipse Capella - practical usage example / demo for SDV community - POSTPONE


Link to Microsoft Teams hybrid sessions: 

Day 1 - Teams Link: https://teams.microsoft.com/l/meetup-join/19%3ameeting_OGU2OTc1NTYtNzBmMi00YjVjLThiNjMtYzYxN2RlN2NlNzRl%40thread.v2/0?context=%7b%22Tid%22%3a%229652d7c2-1ccf-4940-8151-4a92bd474ed0%22%2c%22Oid%22%3a%22558841d0-f784-4c86-a36a-4a4c3bdd8948%22%7d


- Besprechungs-ID: 356 538 251 738
- Kennung: YnnQbb



Day 2 - Teams link: https://teams.microsoft.com/l/meetup-join/19%3ameeting_NGEwNDQzMDctMTJkMi00ZDNhLWI1MTMtODBiODE4Y2FhY2Uw%40thread.v2/0?context=%7b%22Tid%22%3a%229652d7c2-1ccf-4940-8151-4a92bd474ed0%22%2c%22Oid%22%3a%22558841d0-f784-4c86-a36a-4a4c3bdd8948%22%7d 


- Besprechungs-ID: 310 987 963 394
- Kennung: tuHsKV

